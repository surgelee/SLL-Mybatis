package com.surge;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class OrderMyBatis {
	
	@Test
	public void selectOrder() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			Order order = session.selectOne("com.surge.OrderMapper.getOrder", 1);
			
			
			System.out.println(order);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	
	@Test
	public void selectOrder2() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			Order order = session.selectOne("com.surge.OrderMapper.getOrder2", 1);
			
			
			System.out.println(order);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	
	@Test
	public void selectOrder3() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			List<Order> orders = session.selectList("com.surge.OrderMapper.getOrder3");
			
			
			System.out.println(orders);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
}
