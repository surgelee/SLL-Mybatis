package com.surge;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class UserMyBatis {
	
	@Test
	public void selectUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			User user = session.selectOne("com.surge.UserMapper.selectUser", 2);
			
			
			System.out.println(user);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void addUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			int user = session.insert("com.surge.UserMapper.insertUser", new User(-1,"MM",23));
			
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void updateUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			int user = session.insert("com.surge.UserMapper.updateUser", new User(12,"GG",23));
			
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void deteleUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			int user = session.insert("com.surge.UserMapper.deleteUser", 12);
			
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	/**
	 * 查询结果集
	 */
	@Test
	public void selectUsers() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			List<User> users = session.selectList("com.surge.UserMapper.selectAllUsers");
			
			
			System.out.println(users);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
}
