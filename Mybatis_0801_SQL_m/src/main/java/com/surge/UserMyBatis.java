package com.surge;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class UserMyBatis {
	
	
	/**
	 * 查询结果集
	 */
	@Test
	public void selectUsers() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			String name = "o";
			name = null;
			
			ConditionUser parameter = new ConditionUser("%"+name+"%", 13, 18);
			
			List<User> users = session.selectList("com.surge.userMapper.getUsers", parameter);
			
			
			System.out.println(users);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
}
