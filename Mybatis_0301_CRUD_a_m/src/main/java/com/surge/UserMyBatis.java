package com.surge;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class UserMyBatis {
	
	@Test
	public void selectUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			UserMapper mapper = session.getMapper(UserMapper.class);
			User user = mapper.selectUser(3);
			
			System.out.println(user);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void addUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			UserMapper mapper = session.getMapper(UserMapper.class);
			int user = mapper.insertUser(new User(-1, "SS", 45));
			
			System.out.println(user);
			
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void updateUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			
			UserMapper mapper = session.getMapper(UserMapper.class);
			int user = mapper.updateUser(new User(13, "SS", 45));
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	@Test
	public void deteleUser() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		// 默认手动提交 需要自动写 openSession(true)
		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			UserMapper mapper = session.getMapper(UserMapper.class);
			int user = mapper.deleteUserById(13);
				
			// 手动提交
			session.commit();
			session.close();
			
			System.out.println(user);
			
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	/**
	 * 查询结果集
	 */
	@Test
	public void selectUsers() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			UserMapper mapper = session.getMapper(UserMapper.class);
			List<User> users = mapper.getAllUser();
			
			System.out.println(users);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
}
