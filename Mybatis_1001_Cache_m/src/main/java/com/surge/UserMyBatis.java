package com.surge;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;


/*
 * 测试缓存
 1. 一级缓存 : session级的缓存 
 	1. 执行了session.clearCache();
 	2. 执行CUD操作
 	3. 不是同一个Session对象
 2. 二级缓存: 是一个映射文件级的缓存
 */
public class UserMyBatis {
	
	
	/**
	 * 查询结果集
	 */
	@Test
	public void testCache1() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		//==========================以下为测试的 代码
		
		SqlSession session = sqlSessionFactory.openSession();
		String statement = "com.surge.userMapper.getUser";
		
		System.out.println("===================一级缓存测试====================================");
		
		User user = session.selectOne(statement, 1);
		System.out.println(user);
		
		System.out.println("=====第一次查询执行SQL======");
		System.out.println("===================一级缓存1 查看执行sql语句次数==================");
		
		user = session.selectOne(statement, 1);	
		System.out.println(user);
		
		System.out.println("=====第二次查询 使用缓存，不执行sql======");
		System.out.println("===================一级缓存2 查看执行sql语句次数==================");
		
		/*
		 *  有三种情况不会使用缓存数据，会重新执行sql语句再缓存
		 * 
		 * //1. 执行了session.clearCache(); 
		 * 
		 * //2. 执行CUD操作 
		 *    session.update("com.surge.userMapper.updateUser", new
		 *                User(1, "Tom", 13)); 
		 *                session.commit();
		 * 
		 * //3. 不是同一个Session对象
		 *      测试发现：使用不同的sqlSessionFactory创建的session，才会重新查询sql。
		 *              这是因为没有在配置文件中  关闭二级缓存标签
		 * 
		 */
		
		session.clearCache(); 
		user = session.selectOne(statement, 1);	
		System.out.println(user);
		System.out.println("=====清除缓存  session.clearCache(); ======");
		System.out.println("===================一级缓存4 查看执行sql语句次数=================");
		//关闭session
		session.close();
		
		SqlSession session2 = sqlSessionFactory.openSession();
		user = session2.selectOne(statement, 1);
		System.out.println(user);
		
		System.out.println("=====使用不同的session，不会使用缓存======");
		System.out.println("===================一级缓存5 查看执行sql语句次数=================");
		
		session.close();
	}
	
	
	/**
	 * 测试二级缓存
	 * 
	 * 打开配置文件中的二级缓存标签
	 * 
	 */
	@Test
	public void testCache2() throws IOException {
		
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		SqlSession session1 = sqlSessionFactory.openSession();
		SqlSession session2 = sqlSessionFactory.openSession();
		
		String statement = "com.surge.userMapper.getUser";
		User user = session1.selectOne(statement, 1);
		// 注意要提交
		session1.commit();
		System.out.println(user);
		
		user = session2.selectOne(statement, 1);
		session2.commit();
		System.out.println(user);
		
		
	}
	
}
