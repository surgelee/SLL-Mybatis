package com.surge;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class ClassesMyBatis {
	
	/**
	 * 级联查询
	 */
	@Test
	public void selectClasses() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			Classes classes = session.selectOne("com.surge.ClassMapper.getClass", 1);
			
			
			System.out.println(classes);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	/**
	 * 分开查询，mybatis 自动执行两次 sql
	 */
	@Test
	public void selectClasses2() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			
			Classes classes = session.selectOne("com.surge.ClassMapper.getClass2", 1);
			
			
			System.out.println(classes);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
	
}
