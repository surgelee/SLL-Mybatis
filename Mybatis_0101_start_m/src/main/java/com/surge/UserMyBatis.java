package com.surge;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserMyBatis {
	
	final Logger logger  =  LoggerFactory.getLogger(UserMyBatis. class );

	public void selectUser() throws IOException{
		
		String resource = "mybatis-config.xml";
	    InputStream inputStream = Resources.getResourceAsStream(resource);
	    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
	    
	    SqlSession session = sqlSessionFactory.openSession();
	    try {
	      User user = session.selectOne("com.surge.UserMapper.selectUser", 2);
	      
	      System.out.println(user);
	      
	      logger.debug(user.toString());
	      
	    } finally {
	      session.close();
	    }
	}

}
