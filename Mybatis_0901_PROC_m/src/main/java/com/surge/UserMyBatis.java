package com.surge;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

public class UserMyBatis {
	
	
	/**
	 * 查询结果集
	 */
	@Test
	public void selectUsers() throws IOException {

		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession session = sqlSessionFactory.openSession();
		try {
			//============================= 具体操作 开始===========================
			Map<String, Integer> parameterMap = new HashMap<String, Integer>();
			parameterMap.put("sexid", 1);
			parameterMap.put("usercount", -1);
			
			session.selectOne("com.surge.userMapper.getUserCount", parameterMap);
			
			Integer result = parameterMap.get("usercount");

			System.out.println(result);
			//============================= 具体操作 结束===========================


		} finally {
			session.close();
		}
	}
	
}
